from flask import Flask, request, jsonify
from flask_cors import CORS

import cv2
import numpy as np
from io import BytesIO
from PIL import Image
import base64
import qimage2ndarray as q2n
from PyQt5 import QtCore

app = Flask(__name__)
app.config['DEBUG'] = True
CORS(app)

@app.route('/suavizarImagen', methods=['POST'])
def suavizar():

    base64_decoded = base64.b64decode(request.form.get('data'))
    image = Image.open(BytesIO(base64_decoded))
    image_np = np.array(image)

    imagenSuavizada = cv2.blur(image_np, (10, 10))

    buffer = QtCore.QBuffer()
    buffer.open(buffer.WriteOnly)
    buffer.seek(0)

    imagenBase64 = q2n.array2qimage(imagenSuavizada)

    imagenBase64.save(buffer, 'PNG', quality=100)
    result = bytes(buffer.data().toBase64()).decode()

    return jsonify({
        'data': result
        }), 200

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5003)
